# README #

### What is this repository for? ###

* This script allows you to check if a password passed as parameter is secure by checking if it is included in a file with unsecure passwords

### How do I get set up? ###

* npm install
* node password_checker.js passwordToCheck

