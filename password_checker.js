const fs = require('fs');
const got = require('got');
const readline = require('readline');

const path = './passwords.txt'
// Contraseña que queremos comprobar
const password = process.argv.slice(2)[0]

// Función para descargar una copia del fichero en local si no la tenemos
async function fetchFile () {
  try {
    // Si no existe el fichero lo descargamos
    if (!fs.existsSync(path)) {
      const file = fs.createWriteStream("passwords.txt")
      const response = await got.stream("https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/xato-net-10-million-passwords.txt")
      response.pipe(file)
    }
  } catch (err) {
    console.error(err)
  }
}

// Función que comprueba si existe o no la contraseña en el fichero
async function checkPassword (){
  let found = false
  return new Promise((resolve, reject) => {
    const file = readline.createInterface({
      input: fs.createReadStream(path),
      output: process.stdout,
      terminal: false
    })
    file.on('line', (line) => {
      if (line === password) {
        found = true
        file.close()
        file.removeAllListeners()
      }
      // console.log(line)
    })
    file.on('close', () => {
      resolve(found)
    })
  })
}

(async () => {
  await fetchFile()

  if (password) {
    console.log(`Checking password ${password}...`)
    try {
      let passwordFound = await checkPassword()
      if (passwordFound) {
        console.log('Password found')
      } else {
        console.log('Password not found')
      }
    } catch (err) {
      console.log(err)
    }
  } else {
    console.log('No password to check')
  }
})()